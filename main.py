import traceback
import urllib.request
from time import sleep
import ntpath
from pathlib import Path
import ctypes  # An included library with Python install.

from bs4 import BeautifulSoup
import urllib.parse


def findFilesRecursive(link):
    searchFile = open("23370.txt", "r")
    folderFound = 0
    videoFormats = [".mp4", ".mkv", ".avi", ".mov",".wmv",".webm"]
    for line in searchFile:
        if line[:-1] == link:
            folderFound = 1
            break
    searchFile.close()
    if folderFound == 0:
        if '.' in link[-12:] and len(link) > 30: #age 50 chilo
            extension = Path(link).suffix.lower()
            if (extension in videoFormats):
                # print("file pawa gese")
                # step for writting a movie into a txt file with link
                filename = Path(link).stem
                filename = urllib.parse.unquote(filename).replace('?','-')
                f = open("Movies23370/" + filename + ".txt", "a")
                f.write(link)
                f.write('\n')
                f.close()
                print(filename)
                # sleep(1)
                # end


        else:
            if link.endswith(('mp4','mkv','avi', 'mov','wmv','webm')) is False:
                page = urllib.request.urlopen(link, timeout=10)
                if page:
                    soup = BeautifulSoup(page, 'html.parser')
                    if soup is not None:
                        links = soup.find("pre").find_all("a")

                        for sublink in links[1:]:
                            sleep(.1)
                            subtext = sublink.next_sibling.getText().strip()
                            if subtext.endswith('-') is False and sublink['href'].endswith(('mp4', 'mkv', 'avi', 'mov', 'wmv', 'webm')) is False:
                                continue
                            newLink = link + sublink['href']
                            #print(newLink)
                            alreadyFound = findFilesRecursive(newLink)
                            if alreadyFound == 1: continue
                            f = open("23370.txt", "a")
                            f.write(newLink)
                            f.write('\n')
                            f.close()
    else:
        # print("File already ache")
        return 1


def mainFunction():
    homeLink = 'http://103.205.233.70/Data/'
    findFilesRecursive(homeLink)


try:
    mainFunction()
except Exception:
    traceback.print_exc()
    # ctypes.windll.user32.MessageBoxW(0, "error ", "too many request!", 1)

finally:
    print("dd")
